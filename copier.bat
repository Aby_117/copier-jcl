@ECHO OFF 

rem Checks if the parameter is empty 
if [%~1] == [] goto copier

rem The user can pass a '-h' parameter for extra help
if %~1 == -h (goto help)

rem Boolean to later check if both the passed parameters are indeed folders
set /a isFolder=1

rem Arg counter
rem Counts the number of parameters the user has passed
set argCount=0
for %%j in (%*) do (

	rem Checks if the parameters passed are folders
	if not exist "%%j\" (
	  set /a isFolder=0
	)

   set /a argCount += 1
)

rem SetLocal enables the updating variables within loops
SetLocal EnableDelayedExpansion

rem Checks if more than one parameter has been passed to the command
if %argCount% GTR 1 goto error2

rem If both the parameters are not folders
rem The main code block executes

if not %isFolder%==1 (
	rem Just to test the parameter
	set "paramTest=%~1"

	rem Check if the file is a '.txt' filetype
	if not "!paramTest:~-4!"==".txt" (
		goto error4 
		set !paramTest!=""
	)

	rem If file does not exist
	if not exist %~1 goto error1

	rem Parameter
	set "arg1=%~1"

	shift

	ECHO ========================================================================================================

	ECHO This program will use the file "!arg1!" to copy contents from a source folder into a destination folder

	ECHO ========================================================================================================

	rem The line below skips a line
	ECHO.

	ECHO The copying process in !arg1!:

	ECHO. 

	::Boolean to tell if anything was copied at all
	set "wasCopied=0"

	rem Initializing word count
	set "wordCount=0"

	rem Setting Filename
	set "file=!arg1!"

	rem Setting a line counter
	set "lineCount=0"

	rem Setting boolean value to tell if everything was copied successfully or not
	rem Initializing it as true
	set "successfulCopy=1"

	rem Defining a variable i in a for loop
	rem for multiple lines use brackets
	rem /F is to open a file and read its contents
	for /F "tokens=*" %%i IN (!arg1!) DO (

		rem counts each line in the file
		set /a lineCount+=1
		rem line is temporary variable which stores the string content of each line
		set "line=%%i"

		rem Shows the user which line is being copied
		ECHO Line: !lineCount!
		ECHO.

		for %%s in (%%i) do (

			rem Counts the number of strings per line, which are folder names in this case
			set /a wordCount+=1

			if !wordCount! EQU 1 (set "copyFolder=%%s") 

			if !wordCount! EQU 2 (set "toBeCopiedInto=%%s")
		)

		rem If source folder doesn't exist give an error for specific line and then move onto the next line
		rem If destination folder doesn't exist then
		if !wordCount!==2 (

			if EXIST !copyFolder! (
				if EXIST !toBeCopiedInto! (

					rem The command below checks whether there is at least one non-hidden file or directory in the folder
					dir /b /a !toBeCopiedInto! | findstr . >nul

					rem If there is, the command succeeds (i.e. returns 0); if not, it fails (i.e. returns a non-zero error code).
					if !errorlevel! equ 0 (

						rem Boolean to check whether the user wants to overwrite a folder
						set /a overwrite=0
						rem User input on whether a file should be overwritten
						set /p toCopy=Do you want to overwrite !toBeCopiedInto!?(Y/N^)

						rem if the user wants to overwrite a file
					  	if "!toCopy!"=="Y" (
					  		xcopy !copyFolder! !toBeCopiedInto! /E 
							set /a wasCopied=1
							set /a overwrite=1
							echo.
							echo Copying from: !copyFolder!
							echo Into: !toBeCopiedInto!
							echo Done^^!
					  	)

					  	rem if the user wants to overwrite a file
						if "!toCopy!"=="y" (
							xcopy !copyFolder! !toBeCopiedInto! /E 
							set /a wasCopied=1
							set /a overwrite=1
							echo.
							echo Copying from: !copyFolder!
							echo Into: !toBeCopiedInto!
							echo Done^^!
						) 

						rem displays if the user has chosen not to overwrite the file
						if !overwrite!==0 (
							ECHO You've chosen not to overwrite !toBeCopiedInto!.
						)		

					rem if the destination folder is empty the program copies the file without asking for 
					rem permission
					) else (
						xcopy !copyFolder! !toBeCopiedInto! /E 
						set /a wasCopied=1
						echo.
						echo Copying from: !copyFolder!
						echo Into: !toBeCopiedInto!
						echo Done^^!
					)

					rem if the destination folder does not exist
					) else (
						echo Error: It seems like the destination folder does not exist on line !lineCount!.
						set /a successfulCopy=0
					)
				) else (
					ECHO Error: Source folder does not exist on line !lineCount!.
					set /a successfulCopy=0
				)
		) else (
			call :error3 "!arg1!"
			set /a successfulCopy=0
		)

		set /a wordCount=0
		ECHO.
	)

		ECHO.

		rem if anything was copied at all
		if !wasCopied! == 1 (
			rem if whatever folders the user had used were copied without errors
			if !successfulCopy!==1 (
				goto success
			) else (
				goto done
			)
		) else (
			call :failure "!arg1!"
			EXIT /B
		)
	)



rem The copier functions takes two inputs
rem Makes sure that both the inputs are folders
rem Copies the files/dirs from one folder into the other
:copier
	ECHO.
	ECHO Please enter the right folders in order for them to be copied.
	ECHO ==================================
	ECHO Folders with spaces in their names can just be inputted as is without quotes.

	ECHO.

	rem the user is prompted to enter a source and destination folder
	set /p source=Enter a source folder: 
	set /p destination=Enter a destination folder:

	ECHO ==================================

	ECHO.

	rem if the source and destination folders exist the program executes properly
	rem or else an approriate error is thrown
	if EXIST "%source%" (
		if EXIST "%destination%" (
			xcopy "%source%" "%destination%" /E 
		) else (
			call :error5 "%destination%" "destination"
			EXIT /B
		)
	) else (
		call :error5 "%source%" "source"
		EXIT /B
	)

	goto success

	EXIT /B

rem If the program is done running successfully
:success
	ECHO Program is done copying everything perfectly^^!
	ECHO.
	ECHO THANKS FOR USING THIS PROGRAM^^!^^!^^! See you soon^^!
	EXIT /B

::if nothing nothing was copied from the file
:failure
	ECHO Sorry, there was nothing to copy in the file: %~1.
	ECHO.
	ECHO THANKS FOR USING THIS PROGRAM^^!^^!^^! See you soon^^!
	EXIT /B

rem If the program encounters a few issues along the way
:done
	ECHO Progam had a few issues in copying some files and folders.
	ECHO Look at the outputs above for each line to determine the issues.
	ECHO.	
	ECHO THANKS FOR USING THIS PROGRAM^^!^^!^^! See you soon^^!
	EXIT /B


rem Help for the user
:help
	ECHO This command opens a file and looks at the source and destination folders mentioned within that file and copies the files and directories from the source folder into the destination folder.
	ECHO Usage: copier.bat filename.txt
	ECHO.
	ECHO File should contain a source and destination folder per line.
	ECHO An example line in a file: src destination
	ECHO.
	ECHO Alternatively, you can pass it just two inputs to copy the contents of the source folder into the destination folder.
	ECHO Usage: copier.bat
	EXIT /B

rem All the possible error 'goto' functions
rem In case the .txt file does not exist
:error1
	ECHO Sorry, it seems like the file that you've input does not exist
	ECHO Makes sure to enter the correct location of the file for example D:\%USERNAME%\Documents\file.txt.
	GOTO :EOF

rem Warns the user that they've passed in too many parameters to the command
:error2 
	ECHO Sorry, its seems like you've passed in too many parameters.
	ECHO You can try the following ways to use the copier command.
	ECHO.
	ECHO Try: copier.bat
	ECHO Try: copier.bat ^<filename^>
	ECHO Enter copier.bat -h for help
	EXIT /B

rem Proper function
rem Tells the user which line in their text file is causing an error and the name of the text file
:error3
	set wordCount=0
	ECHO Error: Cannot copy from the folder provided into another folder because there are no folders or too many folders to copy into.
	ECHO If you have a folder name with spaces in-between like (Folder 1). Input it as "Folder 1" in the file (%~1).
	EXIT /B

rem If the user passes a parameter without a '.txt' extension
:error4 
	ECHO Sorry, you've passed in an invalid parameter: %~1.
	ECHO The file is invalid.
	ECHO Make sure it is a text file with a ".txt" extension.
	ECHO If you're trying to copy a folder just use the "copier.bat" command, and follow the instructions on-screen.
	GOTO :EOF


::Incase the source or destination folder does not exist
:error5
	ECHO It seems like the %~2 folder "%~1%" that you've input does not exist.
	ECHO Makes sure to enter the correct location of the folder. For example D:\%USERNAME%\Documents\%~2_folder
	EXIT /B


PAUSE
