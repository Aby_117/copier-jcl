# Copier-JCL

## Description
This is a simple project I had to build for my Operating System's course. This script, named copy_task, copies the contents of source folders (and their subfolders) to specified destination folders. It reads input from a text file or prompts the user if no command line parameter is provided.

## Installation
No installation required

## Usage
Go into the project directory and run the following:

`copier filename.txt`
or
`copier`

## Example
`copier list.txt`

Contents of list.txt:
```
source1 dest1
C:\Users\source2 D:\testing\dest2
```

## Contributing
Since these are my personal projects, there have been no other contributors to them.

## Authors and acknowledgment
Aby

## License
No Public License

## Project status
Complete. No Bugs.
