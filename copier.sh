#!/bin/bash
null=0

#The function throws an error message if a source/destination dir does not exist
#It takes two parameters both are strings
error_missingDir() {
	echo ""
	echo "Sorry, it seems like $1 is not a valid $2 directory."
	echo "Please make sure you've passed in its correct location."
	echo ""
	echo "For example, if the directiory is in '/home/"$USER"/"$1"', and you're just passing in '$1' then the program won't detect the directory."
}

#If the user passes in no parameters the following if block will prompt them for a parameter with instructions
if [ $# -eq $null ]
    then
    	read -p "Enter a source directory: " source_dir
    	read -p "Enter a destination directory: " dest_dir

    	#Checking if both the parameters are directories before copying contents from the source into the destination
	    if [[ -d $source_dir ]]
	      then
	      	if [[ -d $dest_dir ]]
	      		then
		      	if [ -z "$(ls -A $dest_dir)" ]; 
		      		then
				        cp $source_dir"/." $dest_dir -r
				        echo "The content from" $source_dir "has been copied into" $dest_dir
				else
			      	while true; do
			      		#Asking for confirmation from the user to overwrite the files
				    	echo "Are you sure you want to overwrite "$dest_dir"? (Enter Y/N)"
						read answer
						if [ "$answer" = "Y" ] || [ "$answer" = "y" ]; 
							then
					        	cp $source_dir"/." $dest_dir -r
					        	echo "The content from" $source_dir "has been copied into" $dest_dir
				        		break
				        fi
						if [ "$answer" = "N" ] || [ "$answer" = "n" ];
							then
								echo "Not overwriting "$dest_dir
								break
						fi
					done
					#Tells the user it's done
					echo ""
					echo "Done Copying!"
				fi
			else 
				error_missingDir $dest_dir "destination"
			fi
		#When incorrect parameters are passed 
	    else 
	    	if [[ ! -d $source_dir ]] && [[ ! -d $dest_dir ]]
	    		then
	    			echo ""
	      			echo "Sorry, it seems like both the directories you've inputted are not directories."
	      			echo "Please make sure you've passed in their correct location."
	      			echo ""
	      			echo "For example, if the directiory is in '/home/$USER/dir', and you're just passing in 'dir' then the program won't detect the directory."
	      			exit 1
	    	else 
				error_missingDir $source_dir "source"
			fi
	    fi

#If the user passes the parameter '-h' they get instructions on how to run the copiermand
elif [ $1 = '-h' ] 
    then        
        echo "This program helps copy files and sub-directories that are in the source directories into the destination directories that are listed within the file that you pass as a parameter."
        echo "Eg: ./copier.sh <parameter>"
        echo "    ./copier.sh file-name.txt"
        echo "Eg: ./copier.sh"
        echo "    Prompts the user for a source and destination folder."
        
#Main Code

#If the parameter is not empty and is not greater than 1
elif [ "$1" != "" ] 
    then 

    #This is if a single parameter passed
	if [ "$#" -eq 1 ];
	  then

	  	let wasCopied=0 

	  	#Checking if the parameter is a text file
	  	if file $1  | grep -q text$;
	  	  then 
			  #Takes in the first user param
			  p=$1

			  #Declaring an iterative variable
			  let i=0

			  #Declare which file is being run
			  echo "Is running $p"

			  #Reads each line of a file and stores it in a variable called line
			  while IFS=" " read -r -u 3 line || [[ -n "$line" ]];
			  do
			  	  echo ""
			      #Initializing the wordcount
			      let wordCount=0

			      #Intializing the directories that'll be used to copy
			      first_dir=''  
			      second_dir=''

			      #Iteration for each line
			      let i+=1

			      #Line it's going through
			      echo "Line $i: $line"

			      #Iterating through each word in a line
			      for word in $line; 
			        do 

			          #Counting the number of words per line
			          let wordCount++

			          if [ $wordCount -eq 1 ]  
			            then
			              first_dir=$word
			          fi

			          if [ $wordCount -eq 2 ]  
			            then
			              second_dir=$word
			          fi
			      	done

			      #This makes it easier for the user to understand what's happening with the code
			      echo "Source Directory:" $first_dir
			      echo "Destination Directory:" $second_dir

			      #The following block copies the src to the destination mentioned within the file only and only if there are two seperate words per line
			      if [ $wordCount -gt 2 ]			       
			      	then
			        echo "There are more than 2 directories described on line $i. "
			      elif [ $wordCount -lt 2 ]
			      	then 
			      	 echo "There are less than 2 directories described on line $i. " 
			      else
			      	#Checing if the src directory exists
			    	if [ ! -d $first_dir"/." ]; 
			    		then
  						 echo "The source directory, "$first_dir", does not seem to exist"
  					#Checing if the destination directory exists
  					elif [ ! -d $second_dir"/." ];
  						then
  						 echo "The destination directory, "$second_dir", does not seem to exist"
					else
						#Checking if the destination dir is empty
						if [ -z "$(ls -A $second_dir)" ]; then
					        cp $first_dir"/." $second_dir -r
					        let wasCopied=1
					        echo "The content from" $first_dir "has been copied into" $second_dir
					    #If the destination dir is not empty
						else
						    while true; do
						    	#Ask user permission to overwrite
						    	echo "Are you sure you want to overwrite "$second_dir"? (Enter Y/N)"
								read answer
								if [ "$answer" = "Y" ] || [ "$answer" = "y" ]; 
									then
										echo "Overwriting "$second_dir
							        	cp $first_dir"/." $second_dir -r
							        	let wasCopied=1
							        	echo "The content from" $first_dir "has been copied into" $second_dir
						        		break
						        fi
								if [ "$answer" = "N" ] || [ "$answer" = "n" ];
									then
										echo "Skipping "$second_dir
										break
								fi
							done
						fi
				    fi
			      fi
			  done 3<$p

			  #If the the something does get copied
			  if [ "$wasCopied" -eq "1" ]
			  	then
			  		echo "Done Copying!"
			  #If nothing gets copied
			  else
			  	echo "Nothing was copied!"
			  fi

		elif file $1 | grep -q empty$
			then 
				echo "Sorry, it seems like the file is empty."
				echo "Use ./copier.sh -h for more information"
		
		#If the file is not a text file
		else
			echo "Sorry, it seems like the file does not exist or the file is not a '.txt' file"
			echo "Use ./copier.sh -h for more information"
		fi
	#When too many parameters are passed 
	else 
      	echo ""
      	echo "Sorry, it seems like you've passed in too many parameters."
      	echo "Use ./copier -h for more information."
      	echo ""
    fi
fi 


